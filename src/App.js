import React, { Component } from "react";
import "./App.css";
// import { scroller } from "react-scroll";

class Form extends Component {
    constructor() {
        super();
        this.state = {
            name: "",
            description: ""
        };
    }
    componentDidMount = _ => {
        if (this.props.last) {
            this.setState(this.props.last);
        }
    };

    handleChange = e => {
        let state = this.state;

        state[e.target.id] = e.target.value;

        this.setState(state);
        this.props.updateLast(state);
    };
    render() {
        let { name, description } = this.state;

        return (
            <div
                style={{
                    background: "#ccc",
                    width: "400px",
                    height: "500px"
                }}
                id={"task_" + this.props.index}
            >
                <h4>Task #{this.props.index}</h4>
                <input
                    placeholder="Name"
                    id="name"
                    onChange={e => this.handleChange(e)}
                    value={name}
                />
                <textarea
                    placeholder="Description"
                    id="description"
                    onChange={e => this.handleChange(e)}
                    value={description}
                />
                <button onClick={this.props.handleAdd}>Add</button>
            </div>
        );
    }
}

class App extends Component {
    constructor() {
        super();
        this.state = {
            tasks: [],
            save: false,
            data: [],
            last: {}
        };
    }
    componentDidMount = _ => {
        this.state.tasks.push(
            <Form
                key={this.state.tasks.length + 1}
                index={this.state.tasks.length + 1}
                handleAdd={this.handleAdd}
                updateLast={this.updateLast}
            />
        );
        this.setState(this.state);
    };
    handleAdd = async _ => {
        let tasks = this.state.tasks;
        tasks.push(
            <Form
                key={this.state.tasks.length + 1}
                index={this.state.tasks.length + 1}
                handleAdd={this.handleAdd}
                last={this.state.last}
                updateLast={this.updateLast}
            />
        );

        await this.setState(this.state);

        let id = "task_" + this.state.tasks.length;
        document
            .getElementById(id)
            .scrollIntoView({ behavior: "smooth", block: "center" });

        // setTimeout(() => {
        // console.log(id);
        // scroller.scrollTo(id, {
        //     duration: 700,
        //     delay: 0,
        //     smooth: "easeInOutQuart"
        // });
        // }, 100);
    };
    handleSave = _ => {
        console.log("Guardar");
        console.log(this.state.data);
    };
    updateLast = last => {
        this.setState({ last });
    };
    render = _ => {
        return (
            <div style={{ padding: "20px" }}>
                <button onClick={this.handleSave}>Save</button>
                <div>{this.state.tasks}</div>
            </div>
        );
    };
}

export default App;
